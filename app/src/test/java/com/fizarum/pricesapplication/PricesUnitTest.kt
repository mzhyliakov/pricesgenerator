package com.fizarum.pricesapplication

import com.fizarum.pricesapplication.data.repositories.PricesRepositoryImpl
import com.fizarum.pricesapplication.domain.entities.Price
import com.fizarum.pricesapplication.domain.entities.PriceDirection
import org.junit.Assert
import org.junit.Test


class PricesUnitTest {

    @Test
    fun pricesAreEquals_isCorrect() {
        val price1 = Price("store", 1.0, PriceDirection.unknown)
        val price2 = Price("store", 1.0, PriceDirection.unknown)

        Assert.assertEquals(price1, price2)
    }

    @Test
    fun pricesAreNotEquals_isCorrect() {
        val price1 = Price("store1", 1.0, PriceDirection.unknown)
        val price2 = price1.copy(marketName = "store2")

        Assert.assertNotEquals(price1, price2)
    }

    @Test
    fun firstPriceWithUnknownDirection_isCorrect() {
        val repo = PricesRepositoryImpl()

        repo.prices("test").take(1).test()
            .assertNoErrors()
            .assertValue {
                PriceDirection.unknown == it.direction
            }
    }

    @Test
    fun isPriceDuplicated_isDuplicatesFound() {
        val repo = PricesRepositoryImpl()

        val price1 = Price("store", 1.0, PriceDirection.unknown)
        val price2 = Price("store", 1.0, PriceDirection.unknown)

        val duplicated = repo.isPriceDuplicated(price1, price2)
        Assert.assertEquals(true, duplicated)
    }
}