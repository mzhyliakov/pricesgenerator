package com.fizarum.pricesapplication.domain.entities

data class Price(val marketName: String, val value: Double, val direction: PriceDirection) {

    override fun equals(other: Any?): Boolean {
        if (other == null || other !is Price) return false
        return marketName == other.marketName && value.equals(other.value) && direction == other.direction
    }

    override fun hashCode(): Int {
        var result = marketName.hashCode()
        result = 31.times(result + value.hashCode())
        result = 31.times(result + direction.hashCode())
        return result
    }
}