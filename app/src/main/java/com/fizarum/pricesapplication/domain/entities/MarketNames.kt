package com.fizarum.pricesapplication.domain.entities

object MarketNames {
    val oil = "Oil"
    val zloty = "Zloty"
    val gold = "Gold"
}