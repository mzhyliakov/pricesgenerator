package com.fizarum.pricesapplication.domain.entities

enum class PriceDirection {
    up,
    down,
    unknown;

    companion object {
        fun getCountOfVariants() = values().size
    }
}