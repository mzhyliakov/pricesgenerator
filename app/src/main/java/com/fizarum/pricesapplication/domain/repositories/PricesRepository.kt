package com.fizarum.pricesapplication.domain.repositories

import com.fizarum.pricesapplication.domain.entities.Price
import io.reactivex.rxjava3.core.Flowable

interface PricesRepository {
    fun prices(marketName: String): Flowable<Price>
}