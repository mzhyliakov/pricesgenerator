package com.fizarum.pricesapplication.app

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.activityViewModels
import com.fizarum.pricesapplication.app.extensions.viewModelFactory
import com.fizarum.pricesapplication.app.viewmodels.PricesViewModel
import com.fizarum.pricesapplication.data.repositories.PricesRepositoryImpl
import com.fizarum.pricesapplication.databinding.FragmentPricesBinding
import com.fizarum.pricesapplication.domain.entities.MarketNames.gold
import com.fizarum.pricesapplication.domain.entities.MarketNames.oil
import com.fizarum.pricesapplication.domain.entities.MarketNames.zloty
import com.jakewharton.rxbinding.view.RxView
import rx.Observable

class PricesFragment : Fragment() {

    private val binding: FragmentPricesBinding by lazy {
        FragmentPricesBinding.inflate(layoutInflater)
    }

    private val viewModel: PricesViewModel by activityViewModels(viewModelFactory {
        PricesViewModel(PricesRepositoryImpl())
    })

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding.lifecycleOwner = this
        binding.viewModel = viewModel
        return binding.root
    }

    private fun button1Event(): Observable<String> =
        RxView.clicks(binding.btOilMarket).map { oil }

    private fun button2Event(): Observable<String> =
        RxView.clicks(binding.btZlotyMarket).map { zloty }

    private fun button3Event(): Observable<String> =
        RxView.clicks(binding.btGoldMarket).map { gold }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        Observable.merge(arrayOf(button1Event(), button2Event(), button3Event()))
            .switchMap { Observable.just(it) }
            //added just for example: if option was selected previously user can't select it again
            .filter { viewModel.marketNameLiveData.value != it }
            .subscribe {
                viewModel.unsubscribe()
                viewModel.subscribe(it)
            }
    }
}