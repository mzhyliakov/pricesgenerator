package com.fizarum.pricesapplication.app.extensions

import android.view.View
import androidx.databinding.BindingAdapter

@BindingAdapter("visible")
fun View.setIsVisible(isVisible: Boolean) {
    val visibilityFlag = if (isVisible) View.VISIBLE else View.GONE
    visibility = visibilityFlag
}