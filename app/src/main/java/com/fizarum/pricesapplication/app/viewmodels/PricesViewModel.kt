package com.fizarum.pricesapplication.app.viewmodels

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Transformations
import androidx.lifecycle.ViewModel
import com.fizarum.pricesapplication.domain.entities.Price
import com.fizarum.pricesapplication.domain.repositories.PricesRepository
import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers
import io.reactivex.rxjava3.disposables.CompositeDisposable
import io.reactivex.rxjava3.schedulers.Schedulers
import java.text.SimpleDateFormat
import java.util.*

class PricesViewModel(private val repo: PricesRepository) : ViewModel() {

    private val compositeDisposable = CompositeDisposable()

    private val updatedPrice = MutableLiveData<Price>()

    private val timeFormatter = SimpleDateFormat("HH:mm:ss", Locale.ROOT)

    val priceLiveData: LiveData<String> = Transformations.map(updatedPrice) { price ->
        if (price != null) {
            val time = Date(System.currentTimeMillis())
            val data =
                "Time: ${timeFormatter.format(time)}\nPrice: ${price.value}\ndirection: ${price.direction.name}"
            Log.d("TAG", data)
            data
        } else {
            null
        }
    }

    val marketNameLiveData: LiveData<String> = Transformations.map(updatedPrice) { price ->
        price?.marketName ?: "n/a"
    }

    fun subscribe(marketName: String) {
        val disposable = repo.prices(marketName)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({
                updatedPrice.value = it
            }, { throwable ->
                onError(throwable)
            })
        compositeDisposable.add(disposable)
    }

    fun unsubscribe() {
        compositeDisposable.clear()
    }

    private fun onError(t: Throwable) {
        unsubscribe()
        updatedPrice.postValue(null)
    }

    override fun onCleared() {
        super.onCleared()
        compositeDisposable.dispose()
    }
}