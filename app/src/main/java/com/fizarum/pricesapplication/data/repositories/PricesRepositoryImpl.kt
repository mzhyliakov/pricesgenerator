package com.fizarum.pricesapplication.data.repositories

import com.fizarum.pricesapplication.domain.entities.Price
import com.fizarum.pricesapplication.domain.entities.PriceDirection
import com.fizarum.pricesapplication.domain.repositories.PricesRepository
import io.reactivex.rxjava3.core.Flowable
import java.util.concurrent.TimeUnit
import kotlin.random.Random

class PricesRepositoryImpl(periodInMillis: Long = 300) : PricesRepository {

    private var previousPrice: Price? = null

    private val priceObservable =
        Flowable.interval(periodInMillis, periodInMillis, TimeUnit.MILLISECONDS)

    override fun prices(marketName: String): Flowable<Price> {
        val first = Flowable.fromCallable { createPriceWithUnknownDirection(marketName) }
        val next = priceObservable
            .flatMap { return@flatMap Flowable.just(createPrice(marketName)) }

        return Flowable
            .concat(first, next)
            .filter { price -> !isPriceDuplicated(price, previousPrice) }
            .doOnNext { previousPrice = it }
    }

    private fun getRandomDirection(): PriceDirection {
        val variantIndex = Random.nextInt(PriceDirection.getCountOfVariants())
        return PriceDirection.values()[variantIndex]
    }

    private fun createPrice(marketName: String): Price {
        return Price(marketName, Random.nextDouble(), getRandomDirection())
    }

    private fun createPriceWithUnknownDirection(marketName: String): Price {
        return Price(marketName, Random.nextDouble(), PriceDirection.unknown)
    }

    fun isPriceDuplicated(price: Price, previous: Price?): Boolean {
        if (previous == null) return false
        return price == previous
    }
}